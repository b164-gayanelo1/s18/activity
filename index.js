console.log('Hello, World!');

let trainer = {};

// Properties 
trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu','Charizard','Squirtle','Bulbasaur'];
trainer.friends = {
	kanto: ['Brock','Misty'],
	hoenn: ['May', 'Max']
}


// Methods
trainer.talk = function() {
	console.log('Pikachu! I choose you!');
}

console.log(trainer);

trainer.talk();


// Constructor
function pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2.2 * level;

	// Methods
		// Will add an object as a target
	this.tackle = function(target) {
		console.log(`${this.name} tacked ${target.name}`)

		// Reduce target health by subtracting and reassigning it's value to the pokemon's attack
		target.health -= this.attack

		console.log(`${target.name}'s health is now reduced to ${target.health}`)

		// If the target's health is less than or equal to 0
		if(target.health <= 0) {
			// invokes the faint method from the target object
			target.faint()
		}
	}	

	this.faint = function() {
		console.log(this.name + ' fainted.')
	}	
}

let pikachu = new pokemon('Pikachu', 15);
console.log(pikachu);

let togepi = new pokemon('Togepi', 8);
console.log(togepi);

let mewtwo = new pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke

togepi.tackle(pikachu);
pikachu.tackle(togepi);
mewtwo.tackle(pikachu);